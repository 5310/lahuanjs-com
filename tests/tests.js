var test = require( 'tape' ).test;
var com = require( '../' );
var Ent = require( 'lahuanjs-ent' );

test("Com tests.", function (t) {
    
    // Create entity.
    
    var ent = new Ent();
    
    // Create component by name.
    
    com( 'a' );
    
    t.equal( com.a.comname, 'a', "Component 'a' created by name." );
    
    // Create component by name and deps.
    
    com( 'b', [com.a]);
    
    t.equal( com.b.comname, 'b', "Component 'b' created by name and depedency." );
    
    ent.add( com.b );
    
    t.ok( ent.a, "Dependency 'a' added with component 'b' to entity." );
    
    // Create component by name and constructor.
    
    var c = function() {
        this.created = false;
    };
    
    com( 'c', c);
    
    t.equal( com.c.comname, 'c', "Component 'c' created by name and constructor function." );
    
    // Create component by name, constructor, and prototype properties object.
    
    var d = function() {};
    d.prototype.time = function() { return new Date().toLocaleTimeString(); };
    //NOTE: Date objects are special and cannot be extended normally hence, another custom constructor.
    
    com( 'd', d, {
        deps: [com.c],
        init: function() {
            this.ent.c.created = "This was recreated on "+this.time();
        }
    } );
    
    t.equal( com.d.comname, 'd', "Component 'd' created by name constructor function, and prototype properties object." );
    
    ent.add ( com.d );
    
    t.equal( ent.d.constructor.comname, 'd', "Component 'd' is an instance of the component constructor 'd'." );
    
    t.ok( ent.c.created, "Dependency 'c' added with component 'd' and prototypal special init method invoked upon 'd's addition to entity." );
    
    // Register component by single `ComLike`.
    
    var e = function() {
        this._ = "This is component e.";
    };
    e.comname = 'e';
    
    com( e );
    
    t.equal( com.e.comname, 'e', "Pre-authored `ComLike` component 'e' registered." );
    
    ent.add ( com.e );
    
    t.equal( ent.e._, "This is component e.", "Component 'c' succesfully added to entity." );
    
    // Register component by list of `ComLike`s.
    
    var f1 = function() {
        this._ = "This is component f1.";
    };
    f1.comname = 'f1';
    var f2 = function() {
        this._ = "This is component f2.";
        this.deps = [f1];
    };
    f2.comname = 'f2';
    var f = [ f1, f2 ];
    
    com( f );
        
    t.ok( com.f1 && com.f2, "Pre-authored `ComLike`s 'f1' and 'f2' registered as an array." );
    
    ent.add ( com.f2 );
    
    t.ok( com.f1, "Dependency 'f1' added with component 'f2' to entity." );
    
    // Try to register a non-unique component name.
    
    t.throws( function() { com( 'e' ); }, true, "Trying to add a component with a name that has been already registered throws an error." );
    
    // Tests done.
    t.end();
    
});