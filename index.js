(function() {
    


    /**
     * An optional utility for creating or registering components for use with `lahuanjs-ent`.
     * 
     * [![Build](http://img.shields.io/travis/lahuan/lahuanjs-com.svg?style=flat)](https://travis-ci.org/lahuan/lahuanjs-com)
     * [![Dependencies](http://img.shields.io/david/lahuan/lahuanjs-com.svg?style=flat)](https://david-dm.org/lahuan/lahuanjs-com)
     * [![License](http://img.shields.io/badge/license-mit-green.svg?style=flat)](https://github.com/lahuan/lahuanjs-com/blob/master/LICENSE)
     * [![Release](http://img.shields.io/badge/release-v0.1.1-orange.svg?style=flat)](https://github.com/lahuan/lahuanjs-com/releases)
     * 
     * @module lahuanjs-com
     * 
     * @alias com
     * 
     * @example
     * 
     *  Basic usage:
     * 
     *  ```js
     * 
     *  // Create component by name.
     *  com( 'a' );
     * 
     *  // Create component by name and deps.
     *  com( 'b', [ com.a ]);
     * 
     *  // Create a component by name, constructor, and extra prototypal properties object.
     * 
     *  var c = function() {
     *      this.prefix = "It is now ";
     *  };
     *  c.prototype.time = function() { return new Date().toLocaleTimeString(); };
     * 
     *  com( 'c', c, {
     *      deps: [ com.b ],
     *      init: function() {
     *          console.log( this.prefix+this.time() );
     *      }
     *  } );
     * 
     *  // Use components.
     *  
     *  var ent = new Ent();
     * 
     *  ent.add( com.c );
     *  // It is now <local time>.
     * 
     *  console.log(ent.has( [ com.a, com.b, com.c ] ));
     *  // true
     *  ```
     *  
     *  Usage in Node:
     *  
     *  ```js
     *  var com = require('lahuanjs-com');
     *  //...
     *  ```
     * 
     *  Usage in the browser via UMD build:
     *  
     *  ```html
     *  <script src="<path>/lahuanjs-com.js"></script>
     *  <script>
     *      var com = lahuanjs.com;
     *      //...
     *  </script>
     *  ```
     */



    /**
     * Easily create valid components or uniquely register pre-made components.
     * 
     * Created or registered components are referenced on this object by properties of the same name as the components, serving as a convenient repository for dereferencing components by their name, which might be necessary for serialization, etc.
     * 
     * Component constructors to be registered must meet the [`ComLike`](https://github.com/lahuan/lahuanjs-ent#module_lahuanjs-ent..ComLike) specifications and the instantiated components the [`comlike`](https://github.com/lahuan/lahuanjs-ent#module_lahuanjs-ent..comlike) specifications, as described in `lahuanjs-ent`.
     * 
     * @param {(string|(ComLike|ComLike[]))} a If creating a component, this would be the component's name. If registering a premade component, this would be the `Comlike` to be registered, or an array thereof.
     * @param {(ComLike[]|function)} [b] If creating a component, this would either be an array of the component's dependencies, or the base constructor of the created `ComLike`. This parameter isn't used for registering components.
     * @param {object} [c] If creating a component with a base constructor, this object would contain all the extra properties that would go on the prototype of the created `ComLike`: dependencies, the special methods, or anything else. This parameter isn't used for registering components.
     * 
     * @alias module:lahuanjs-com
     * 
     * @throws {Error} Will throw an exception if a component's name is not unique.
     * 
     * @returns {(ComLike|ComLike[])} Returns the created or registered `ComLike` object or an array thereof.
     */
    var com = function( a, b, c ) {
        
        var register = function( c ) {
            if ( com[c.comname] && com[c.comname] !== c ) throw new Error( "NonUniqueComnameError: " + c.comname + " already registered to a different ComLike." );
            else com[c.comname] = c;
        };
        
        if ( typeof a === 'string' ) {
            // If creating a component by name.
            
            var name = a;
            
            var component;
            
            if ( b ) {
                if ( typeof b === 'function' ) {
                    // If constructor given.
                    
                    var constructor = b;
                    
                    component = function() {
                        constructor.call( this );
                    };
                    component.comname = name;
                    component.prototype = Object.create( constructor.prototype );
                    component.prototype.constructor = component;
                    
                    if ( c ) {
                        // If extra protoype properties object given.
                        // Add all own properties on said object to the constructed prototype of the component being created.
                        var prototypes = c;
                        var prototypesOwnPropertyNames = Object.getOwnPropertyNames( prototypes );
                        for ( var i in prototypesOwnPropertyNames ) {
                            var propertyName = prototypesOwnPropertyNames[i];
                            component.prototype[propertyName] = prototypes[propertyName];
                        }
                    }
                    
                } else {
                    // If dependency given.
                    
                    var deps = b;
                    
                    component = function() {};
                    component.comname = name;
                    
                    component.prototype.deps = deps;
                    
                }
            } else {
                // If only name given.
                
                component = function() {};
                component.comname = name;
                
            }
            
            // Register created component.
            register( component );
            
            // Return created component.
            return component;
            
            
        } else {
            // If registering a component.
            
            // If a single component is supplied, wrap it into an array.
            if ( ! ( a instanceof Array ) ) a = [a];
            var components = a;
            
            // Register the components.
            for ( var i in components ) {
                register( components[i] );
            }
            
            // Return registered components.
            return components;
            
        }
        
    };
    
    
    
    /*
     * The export object.
     */
    module.exports = com;
    
    
    
}).call(this);