<a name="module_lahuanjs-com"></a>
#lahuanjs-com
An optional utility for creating or registering components for use with `lahuanjs-ent`.

[![Build](http://img.shields.io/travis/lahuan/lahuanjs-com.svg?style=flat)](https://travis-ci.org/lahuan/lahuanjs-com)
[![Dependencies](http://img.shields.io/david/lahuan/lahuanjs-com.svg?style=flat)](https://david-dm.org/lahuan/lahuanjs-com)
[![License](http://img.shields.io/badge/license-mit-green.svg?style=flat)](https://github.com/lahuan/lahuanjs-com/blob/master/LICENSE)
[![Release](http://img.shields.io/badge/release-v0.1.1-orange.svg?style=flat)](https://github.com/lahuan/lahuanjs-com/releases)

##Usage

Basic usage:

```js

// Create component by name.
com( 'a' );

// Create component by name and deps.
com( 'b', [ com.a ]);

// Create a component by name, constructor, and extra prototypal properties object.

var c = function() {
    this.prefix = "It is now ";
};
c.prototype.time = function() { return new Date().toLocaleTimeString(); };

com( 'c', c, {
    deps: [ com.b ],
    init: function() {
        console.log( this.prefix+this.time() );
    }
} );

// Use components.

var ent = new Ent();

ent.add( com.c );
// It is now <local time>.

console.log(ent.has( [ com.a, com.b, com.c ] ));
// true
```

Usage in Node:

```js
var com = require('lahuanjs-com');
//...
```

Usage in the browser via UMD build:

```html
<script src="<path>/lahuanjs-com.js"></script>
<script>
    var com = lahuanjs.com;
    //...
</script>
```

##API
<a name="exp_module_lahuanjs-com"></a>
###com(a, [b], [c]) ⏏
Easily create valid components or uniquely register pre-made components.

Created or registered components are referenced on this object by properties of the same name as the components, serving as a convenient repository for dereferencing components by their name, which might be necessary for serialization, etc.

Component constructors to be registered must meet the [`ComLike`](https://github.com/lahuan/lahuanjs-ent#module_lahuanjs-ent..ComLike) specifications and the instantiated components the [`comlike`](https://github.com/lahuan/lahuanjs-ent#module_lahuanjs-ent..comlike) specifications, as described in `lahuanjs-ent`.

####Params

- _a_ `string` | `ComLike` | `Array.<ComLike>` - If creating a component, this would be the component's name. If registering a premade component, this would be the `Comlike` to be registered, or an array thereof.  
- _\[b\]_ `Array.<ComLike>` | `function` - If creating a component, this would either be an array of the component's dependencies, or the base constructor of the created `ComLike`. This parameter isn't used for registering components.  
- _\[c\]_ `object` - If creating a component with a base constructor, this object would contain all the extra properties that would go on the prototype of the created `ComLike`: dependencies, the special methods, or anything else. This parameter isn't used for registering components.  


####Type

`Error`  

####Returns

`ComLike` | `Array.<ComLike>` - Returns the created or registered `ComLike` object or an array thereof.  


